# Generated by Django 4.0.4 on 2022-05-26 18:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AcceptOrder',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_id', models.CharField(max_length=10)),
                ('doctor_id', models.CharField(max_length=10)),
                ('status', models.BooleanField()),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
