from ssl import create_default_context
from telnetlib import STATUS
from django.db import models

# Create your models here.
class AcceptOrder(models.Model) :
    order_id = models.CharField(max_length=10)
    doctor_id = models.CharField(max_length=10)
    status = models.BooleanField()
    date = models.DateTimeField(auto_now_add=True)

    def to_json(self) :
        return {
            "id" : self.id,
            "fields" : {
                "order_id"  : self.order_id,
                "doctor_id"  : self.doctor_id,
                "status"  : self.status,
                "date"  : self.date
            }
        }

class Order(models.Model) :
    doctor_id = models.CharField(max_length=10)
    patient_id = models.CharField(max_length=10)
    status = models.CharField(max_length=20)
    date = models.DateField()
    created_on = models.DateTimeField(auto_now_add=True)
    no_antrian = models.CharField(max_length=10)
    keluhan = models.CharField(max_length=1024)
    riwayat = models.CharField(max_length=1024)

    def to_json(self):
        return {
            "id" : self.id,
            "fields" : {
                "patient_id"  : self.patient_id,
                "doctor_id"  : self.doctor_id,
                "status"  : self.status,
                "date"  : self.date,
                "created_on" : self.created_on,
                "no_antrian" : self.no_antrian,
                "keluhan" : self.keluhan,
                "riwayat" : self.riwayat
            }
        }

    