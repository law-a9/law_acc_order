from django.urls import path
from order import views

app_name="order"

urlpatterns = [
    path('acc-order', views.get_acc_order, name="get_acc_order"),
    path('accept', views.post_acc_order, name="accept_order"),
    path('cancel', views.cancel_acc_order, name="cancel_order"),
    path('', views.get_order, name="get_order"),
    path('create', views.post_create_order, name="create_order"),
]